const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: [ 'whatwg-fetch', './src/index.js' ],
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'bundle.js?[hash]',
    publicPath: '/dist/'
  },
  module: {
    rules: [
      {
        test: /\.html?$/,
        loader: 'raw-loader'
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        include: [
          path.resolve(__dirname, 'src')
        ],
        use: {
          loader: 'babel-loader',
          query: {
            presets: [ 'es2015' ],
            plugins: [ 'transform-object-rest-spread' ]
          }
        }
      },
      {
        test: /\.(scss|css)$/,
        loader: 'style-loader!css-loader?url=false!sass-loader?indentaedSyntax'
      },
      {
        test: /\.vue$/,
        use: {
          loader: 'vue-loader',
          options: {
            loaders: {
              scss: 'vue-style-loader!css-loader!sass-loader',
              sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
              js: 'babel-loader?{"presets":["es2015"],"plugins": ["transform-object-rest-spread"]}',
            }
          }
        }
      }
    ]
  },
  externals: {
    jQuery: '$',
    moment: 'moment',
    loadash: '_',
    document: 'document'
  },
  resolve: {
    alias: {
      components: path.join(__dirname, './src/components'),
      charts: path.join(__dirname, './src/charts'),
      store: path.join(__dirname, './src/store'),
      lib: path.join(__dirname, './src/lib'),
      api: path.join(__dirname, './src/api'),
      utils: path.join(__dirname, './src/utils'),
      scss: path.join(__dirname, './src/assets/scss'),
      template: path.join(__dirname, './src/template'),
      'request$': path.join(__dirname, './src/api/request.js'),
      'vue$': 'vue/dist/vue.common.js'
    }
  },
  devServer: {
    host: '0.0.0.0',
    port: 9876,
    historyApiFallback: true,
    noInfo: false,
    proxy: {
      '/images': 'http://localhost:9876/dist',
      '/fonts': 'http://localhost:9876/dist',
    }
  },
  performance: {
    hints: false
  },
  devtool: '#source-map',
  plugins: [
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, './src/assets/images'),
        to: './images'
      },
      {
        from: path.join(__dirname, './src/assets/fonts'),
        to: './fonts'
      },
      {
        from: './index.html',
        to: '.'
      }
    ])
  ]

};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '\"production\"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
};
