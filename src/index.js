import Vue from 'vue';
import VueHighcharts from 'vue-highcharts';
import Highcharts from 'highcharts/highstock';
import Mint from 'mint-ui';

import 'mint-ui/lib/style.css';
import style from 'scss/index.scss';
import router from 'components/routes.js';

Vue.use(Mint);
Vue.use(VueHighcharts, { Highcharts });

new Vue({
  router,
  template: `
    <router-view></router-view>
  `
}).$mount('#app');
