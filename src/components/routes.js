import Vue from 'vue';
import VueRouter from 'vue-router';

import Main from 'components/default/Main.vue';
import Dashboard from 'components/Dashboard.vue';
import Components from 'components/Components.vue';
import Info from 'components/Info.vue';

Vue.use(VueRouter);

const routes =  [
  {
    path: '/',
    component: Main,
    redirect: '/choice',
    children: [
      { path: '/dashboard', name: 'dashboard', component: Dashboard },
      { path: '/components', name: 'components', component: Components },
      { path: '/info', name: 'info', component: Info },
    ]
  },
  {
    path: '/choice',
    component: Info
  }
];

export default new VueRouter({
  mode: 'hash',
  routes
});
