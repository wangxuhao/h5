import Highcharts from 'highcharts';

export const pie = {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie',
        marginTop: 0,
        height: 200,
        marginTop: 50,
    },
    title: {
        text: '组合配置比例',
        style: { "fontSize": "14px", "fontSize": "1.4rem" },
        align: 'left',
        marginTop: -10
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                }
            }
        }
    },
    series: [{
        name: 'Brands',
        data: [
            { name: 'IE', y: 56.33, },
            { name: 'Chrome', y: 24.03, },
            { name: 'Firefox', y: 10.38, },
            { name: 'Safari', y: 4.77, },
            { name: 'Opera', y: 0.91, },
            { name: 'Undetectable', y: 0.2, }
        ]
    }],
    colors: [
        'rgb(15, 72, 127)', 'rgb(77, 134, 189)', 'rgb(88, 145, 200)', 'rgb(113, 170, 225)', 'rgb(149, 206, 255)', 'rgb(196, 253, 255);'
    ],
    credits: {
        text: '',
        href: '',
    },

}
